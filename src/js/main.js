const menuButtons = document.querySelectorAll('nav div');
const navMenu = document.querySelector('.nav__list');


menuButtons.forEach( item => 
    item.addEventListener('click', e => {
        

        if(menuButtons[1].classList.contains('invisible')) {
            menuButtons[1].classList.toggle('invisible')
            menuButtons[0].style.opacity = '0'
            navMenu.style.display = 'block';

        }
        else {
            menuButtons[1].classList.add('invisible')
            navMenu.style.display = 'none';
            menuButtons[0].style.opacity = '1'
        }        
    }))