const { src, dest, series, parallel, watch } = require("gulp");
const gulpSass = require("gulp-sass")(require("sass"));
const browsersync = require("browser-sync").create();
const cssmin = require("gulp-cssnano");
const jsmin = require("gulp-js-minify");
const clean = require("gulp-clean");
const autoPrefixer = require("gulp-autoprefixer");
const cleancss = require("gulp-clean-css");
const concat = require("gulp-concat");
// const imagemin = require('gulp-imagemin');



const devFolder = "dist";


function scss(f) {
  src("src/scss/*.scss")
    .pipe(
      gulpSass().on("error", () => {
        console.log("Error in sass code!");
      })
    )
    .pipe(cssmin())
    .pipe(dest(devFolder));
    f();
}


function js(f) {
  src("src/js/*.js")
    .pipe(jsmin())
    .pipe(dest(devFolder));
    f();
}



function watcher() {
  browsersync.init({
    server: {
      baseDir: '.',
    },
  });
  watch('*.html').on('change', browsersync.reload)
  watch("src/**/*.*", series(scss)).on("change", browsersync.reload);
}


function cleaner(f) {
  src('./dist', {read: false})
  .pipe(clean());
  f();
}

function buildcss(f) {
  src("src/scss/*.scss")
    .pipe(gulpSass())
    .pipe(autoPrefixer({cascade: false
    }))
    .pipe(cleancss({compatibility: 'ie8'}))
    .pipe(cssmin())
    .pipe(dest(devFolder));
    f();
}


// function img() {
//   return src("src/img/**/*.*") 
//     .pipe(imagemin())
//     .pipe(dest('dist/img'))
// }


function buildjs(f) {
  src("src/js/*.js")
    .pipe(concat('script.min.js'))
    .pipe(jsmin())
    .pipe(dest(devFolder))
  f();  
}

exports.buildjs = buildjs
// exports.img = img;
exports.cleaner = cleaner;
exports.buildcss = buildcss;
exports.js = js;
exports.scss = scss;

exports.dev = series( scss, js, watcher);
exports.build = series(cleaner, buildcss, buildjs);